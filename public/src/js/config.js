'use strict';

// Setting up Google Analytics via angulartics app
angular.module('insight').config(function ($analyticsProvider) {
    $analyticsProvider.firstPageview(true); /* Records pages that don't use $state or $route */
    $analyticsProvider.withAutoBase(true);  /* Records full path */
});

//Setting up route
angular.module('insight').config(function ($routeProvider) {
    $routeProvider.
        when('/block/:blockHash', {
            templateUrl: '/views/block.html',
            title: 'Bitcoin Block '
        }).
        when('/block-index/:blockHeight', {
            controller: 'BlocksController',
            templateUrl: '/views/redirect.html'
        }).
        when('/tx/:txId/:v_type?/:v_index?', {
            templateUrl: '/views/transaction.html',
            title: 'Bitcoin Transaction '
        }).
        when('/', {
            templateUrl: '/views/index.html',
            title: ''
        }).
//        when('/blocks', {
//            templateUrl: '/views/block_list.html',
//            title: 'Bitcoin Blocks mined Today'
//        }).
        when('/blocks', {
            templateUrl: '/views/block_headers.html',
            title: 'Block explorer'
        }).
        when('/blocks-by-pool/:poolName', {
            templateUrl: '/views/block_headers.html',
            title: 'Bitcoin Blocks mined by '
        }).
        when('/blocks-date/:blockDate/:startTimestamp?', {
            templateUrl: '/views/block_list.html',
            title: 'Bitcoin Blocks mined '
        }).
        when('/address/:addrStr', {
            templateUrl: '/views/address.html',
            title: 'Bitcoin Address '
        }).
        when('/status', {
            templateUrl: '/views/status.html',
            title: 'Status'
        }).
        when('/news', {
            templateUrl: '/views/news.html',
            title: 'News'
        }).
        when('/currencies', {
            templateUrl: '/views/currencies.html',
            title: 'Currencies'
        }).
        when('/markets', {
            templateUrl: '/views/candlesticks.html',
            title: 'Markets'
        }).
        when('/pools', {
            templateUrl: '/views/pools.html',
            title: 'Mining pools'
        })
        .otherwise({
            templateUrl: '/views/404.html',
            title: 'Error'
        });
});

//Setting HTML5 Location Mode
angular.module('insight')
    .config(function ($locationProvider) {
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
    })
    .run(function ($rootScope, $route, $location, $routeParams, $anchorScroll, ngProgress, gettextCatalog) {
        gettextCatalog.currentLanguage = 'en';
        $rootScope.$on('$routeChangeStart', function () {
            ngProgress.start();
        });

        $rootScope.$on('$routeChangeSuccess', function () {
            ngProgress.complete();

            //Change page title, based on Route information
            $rootScope.titleDetail = '';
            $rootScope.title = $route.current.title;
            $rootScope.isCollapsed = true;
            $rootScope.currentAddr = null;

            $location.hash($routeParams.scrollTo);
            $anchorScroll();
        });
    });
