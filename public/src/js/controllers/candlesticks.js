'use strict';

angular.module('insight.candlesticks', ["highcharts-ng"]).controller('CandlesticksController',
    function ($scope, $rootScope, $routeParams, $location, $cookies, $modal, Global, Candlesticks, Orderbook, Trades, Ticker) {
        $scope.global = Global;

        $rootScope.meta_keywords = 'candlestick, candles, charts, bitcoin, btc, bitcoin price, bitcoin price graph, bitcoin markets';
        $rootScope.meta_description = 'Live Bitcoin market analysis tools. Candlesticks for BTC/USD and BTC/EUR pairs for various exchanges.';

        $scope.loading_chart = false;
        $scope.loading_orderbook = false;
        $scope.loading_trades = false;
        $scope.market = $cookies.candlesticks_market || 'bitstamp';
        $scope.currency = $cookies.candlesticks_currency || 'usd';
        $scope.period = $cookies.candlesticks_period || 'day';
        $scope.candlesticks = [];
        $scope.bids = [];
        $scope.asks = [];
        $scope.trades = [];
        $scope.ticker = {loading: true };
        $scope.sma_period = +$cookies.sma_period || 10;
        $scope.show_open_sma = !!$cookies.show_open_sma || true;
        $scope.show_close_sma = !!$cookies.show_close_sma || true;

        $scope.show_open_ema = !!$cookies.show_open_ema || true;
        $scope.show_close_ema = !!$cookies.show_close_ema || true;
        $scope.smooth_ema = +$cookies.smooth_ema || 0.1;

        $scope.orderbook_price_grouping_step = +$cookies.orderbook_price_grouping_step || 0.01;
        $scope.orderbook_price_grouping_steps_available = [0.01, 0.1, 1, 10, 100];
        $scope.orderbook_count = +$cookies.orderbook_count || 10;
        $scope.trade_count = +$cookies.trade_count || 10;

        $scope.openChartSettings = function () {

            var modalInstance = $modal.open({
                templateUrl: 'chartSettings.html',
                controller: 'ChartSettingsController',
                size: 'sm',
                resolve: {
                    show_open_sma: function () { return $scope.show_open_sma; },
                    show_close_sma: function () { return $scope.show_close_sma; },
                    sma_period: function () { return $scope.sma_period; },
                    
                    show_open_ema: function () { return $scope.show_open_ema; },
                    show_close_ema: function () { return $scope.show_close_ema; },
                    smooth_ema: function () { return $scope.smooth_ema; },

                    orderbook_price_grouping_step: function () { return $scope.orderbook_price_grouping_step; },
                    orderbook_price_grouping_steps_available: function () {
                        return $scope.orderbook_price_grouping_steps_available; },
                    orderbook_count: function () { return $scope.orderbook_count; },
                    trade_count: function () { return $scope.trade_count; }
                }
            });

            modalInstance.result.then(function (result) {
                $cookies.show_open_sma = $scope.show_open_sma = result.show_open_sma;
                $cookies.show_close_sma = $scope.show_close_sma = result.show_close_sma;
                $cookies.sma_period = $scope.sma_period = result.sma_period;

                $cookies.show_open_ema = $scope.show_open_ema = result.show_open_ema;
                $cookies.show_close_ema = $scope.show_close_ema = result.show_close_ema;
                $cookies.smooth_ema = $scope.smooth_ema = result.smooth_ema;

                $cookies.orderbook_price_grouping_step = $scope.orderbook_price_grouping_step = result.orderbook_price_grouping_step;
                $cookies.orderbook_count = $scope.orderbook_count = result.orderbook_count;
                $cookies.trade_count = $scope.trade_count = result.trade_count;

                // TODO: move this to watchers
                update_smas();
                update_smas_visibility();

                update_emas();
                update_emas_visibility();

                update_trades();
                update_orderbook();
            }, function () {
            });
        };

        $scope.candlestick_cache = {
            btce: {
                'usd': {
                    'minute': {updated: null, data: []},
                    '5minutes': {updated: null, data: []},
                    '15minutes': {updated: null, data: []},
                    '30minutes': {updated: null, data: []},
                    'hour': {updated: null, data: []},
                    '4hours': {updated: null, data: []},
                    '8hours': {updated: null, data: []},
                    '12hours': {updated: null, data: []},
                    'day': {updated: null, data: []},
                    '10days': {updated: null, data: []},
                    '20days': {updated: null, data: []}
                },
                'eur': {
                    'minute': {updated: null, data: []},
                    '5minutes': {updated: null, data: []},
                    '15minutes': {updated: null, data: []},
                    '30minutes': {updated: null, data: []},
                    'hour': {updated: null, data: []},
                    '4hours': {updated: null, data: []},
                    '8hours': {updated: null, data: []},
                    '12hours': {updated: null, data: []},
                    'day': {updated: null, data: []},
                    '10days': {updated: null, data: []},
                    '20days': {updated: null, data: []}
                }
            },
            hitbtc: {
                'usd': {
                    'minute': {updated: null, data: []},
                    '5minutes': {updated: null, data: []},
                    '15minutes': {updated: null, data: []},
                    '30minutes': {updated: null, data: []},
                    'hour': {updated: null, data: []},
                    '4hours': {updated: null, data: []},
                    '8hours': {updated: null, data: []},
                    '12hours': {updated: null, data: []},
                    'day': {updated: null, data: []},
                    '10days': {updated: null, data: []},
                    '20days': {updated: null, data: []}
                },
                'eur': {
                    'minute': {updated: null, data: []},
                    '5minutes': {updated: null, data: []},
                    '15minutes': {updated: null, data: []},
                    '30minutes': {updated: null, data: []},
                    'hour': {updated: null, data: []},
                    '4hours': {updated: null, data: []},
                    '8hours': {updated: null, data: []},
                    '12hours': {updated: null, data: []},
                    'day': {updated: null, data: []},
                    '10days': {updated: null, data: []},
                    '20days': {updated: null, data: []}
                }
            },
            bitstamp: {
                'usd': {
                    'minute': {updated: null, data: []},
                    '5minutes': {updated: null, data: []},
                    '15minutes': {updated: null, data: []},
                    '30minutes': {updated: null, data: []},
                    'hour': {updated: null, data: []},
                    '4hours': {updated: null, data: []},
                    '8hours': {updated: null, data: []},
                    '12hours': {updated: null, data: []},
                    'day': {updated: null, data: []},
                    '10days': {updated: null, data: []},
                    '20days': {updated: null, data: []}
                },
                'eur': {
                    'minute': {updated: null, data: []},
                    '5minutes': {updated: null, data: []},
                    '15minutes': {updated: null, data: []},
                    '30minutes': {updated: null, data: []},
                    'hour': {updated: null, data: []},
                    '4hours': {updated: null, data: []},
                    '8hours': {updated: null, data: []},
                    '12hours': {updated: null, data: []},
                    'day': {updated: null, data: []},
                    '10days': {updated: null, data: []},
                    '20days': {updated: null, data: []}
                }
            }
        };

        $scope.markets_available = [
            {
                id: 'btce',
                title: 'BTCE',
                currencies: ['usd', 'eur']
            },
            {
                id: 'hitbtc',
                title: 'HitBTC',
                currencies: ['usd', 'eur']
            },
            {
                id: 'bitstamp',
                title: 'BitStamp',
                currencies: ['usd']
            }
        ];

        $scope.graph_modes_available = [
            {id: 'candlestick', title: 'Candlestick'},
            {id: 'ohlc', title: 'Bar'},
            {id: 'spline', title: 'Line'},
//            {id: 'bar', title: 'Bar'},
//            {id: 'scatter', title: 'Scatter'}
        ];



        angular.forEach(['market', 'currency', 'period'], function (key) {
            $scope.$watch(key, function (new_value, old_value) {
                if (new_value != old_value) {
                    $cookies['candlesticks_' + key] = new_value;
                    $scope.list();
                }
            });
        });

        $scope.$watch('chartConfig.series[0].type', function (new_value, old_value) {
            $cookies.candlesticks_graph_mode = new_value;
        });

        $scope.$watch('market', function (new_value, old_value) {
            $scope.currentcies_availabe = [];
            for (var i=0; i < $scope.markets_available.length; i++) {
                if ($scope.markets_available[i].id == $scope.market) {
                    $scope.currencies_available = $scope.markets_available[i].currencies;
                    if (-1 == $scope.currencies_available.indexOf($scope.currency)) {
                        $scope.currency = $scope.currencies_available[0];
                    }
                    return;
                }
            }
        });

        angular.forEach(['market', 'currency'], function (key) {
            $scope.$watch(key, function (new_value, old_value) {
                if (new_value != old_value) {
                    $scope.bids = [];
                    $scope.asks = [];
                    $scope.trades = [];
                    update_orderbook();
                    update_trades();
                    $scope.ticker = {loading: true };
                    update_ticker();
                }
            });
        });

        function update_trades() {
            $scope.loading_trades = true;
//            if ($scope.market == 'bitstamp') {
//                $scope.trades = [];
//                return;
//            }
            (function (market, currency) {
                Trades.get({
                        market: $scope.market,
                        currency: $scope.currency,
                        limit: $scope.trade_count
                    },
                    function (res) {
                        if ($scope.market != market || $scope.currency != currency)
                            return;
                        $scope.loading_trades = false;
                        $scope.trades = res.trades;
                    }
                );
            })($scope.market, $scope.currency);
        }

        function update_ticker() {
            (function (market, currency) {
                Ticker.get({
                        market: $scope.market,
                        currency: $scope.currency
                    },
                    function (res) {
                        if ($scope.market != market || $scope.currency != currency)
                            return;
                        $scope.ticker = res.ticker;
                    }
                );
            })($scope.market, $scope.currency);
        }

        function update_orderbook(force) {
//            if ($scope.market == 'bitstamp') {
//                $scope.bids = [];
//                $scope.asks = [];
//                return;
//            }
            $scope.loading_orderbook = true;
            (function (market, currency) {
                Orderbook.get({
                        market: market,
                        currency: currency,
                        limit: $scope.orderbook_count,
                        grouping_price_step: $scope.orderbook_price_grouping_step
                    },
                    function (res) {
                        if ($scope.market != market || $scope.currency != currency)
                            return;
                        $scope.loading_orderbook = false;
                        $scope.bids = res.bids;
                        $scope.asks = res.asks;
                    }
                );
            })($scope.market, $scope.currency);
        }

        var orderbook_interval_task = setInterval(update_orderbook, 10000); // TODO: optimize; async errors are possible here
        var trades_interval_task = setInterval(update_trades, 10000);
        var ticker_interval_task = setInterval(update_ticker, 1000);
        update_orderbook();
        update_trades();
        update_ticker();

        $scope.$on("$destroy", function handler() {
            clearInterval(orderbook_interval_task);
            clearInterval(trades_interval_task);
            clearInterval(ticker_interval_task);
        });

        function get_ema (candles, i, metric_index) {
            var sma = get_sma(candles, i, metric_index);
            var multiplier = 2 / ($scope.sma_period + 1);
            // TODO: memoize
            var prev_ema = candles[i-1] ? get_ema (candles, i-1, metric_index) : candles[i][metric_index];
            return (candles[i][metric_index] - prev_ema) * multiplier + prev_ema;
            // previous implementation was as follows
            // return $scope.smooth_ema * candles[i][metric_index] + (1.0 - $scope.smooth_ema) * (candles[i-metric_index] ? candles[i-1][metric_index] : candles[i][metric_index]);
        }

        function update_emas () {
            var open_ema_data = [];
            var close_ema_data = [];
            var candles = $scope.chartConfig.series[0].data;
            for (var i = 0; i < candles.length; i++) {
                open_ema_data.push([
                    candles[i][0], // simply pass the same time
                    get_ema(candles, i, 1) // [1] stands for `open` in t, ohlc // [1] stands for `open` in t, ohlc
                ]);
                close_ema_data.push([
                    candles[i][0], // simply pass the same time
                    get_ema(candles, i, 4) // [4] stands for `close` in t, ohlc
                ]);
            }
            $scope.chartConfig.series[4].data = open_ema_data;
            $scope.chartConfig.series[5].data = close_ema_data;
        }

        function get_sma (candles, i, metric_index) {
            var sum = 0;
            for (var j = i; j < Math.min(candles.length, i + $scope.sma_period); j++) {
                sum += candles[j][metric_index];
            }
            return sum / Math.min(j - i, $scope.sma_period);
        }

        function update_smas () {
            var open_sma_data = [];
            var close_sma_data = [];
            var candles = $scope.chartConfig.series[0].data;
            for (var i = 0; i < candles.length; i++) {
                open_sma_data.push([
                    candles[i][0], // simply pass the same time
                    get_sma(candles, i, 1) // [1] stands for `open` in t, ohlc
                ]);
                close_sma_data.push([
                    candles[i][0], // simply pass the same time
                    get_sma(candles, i, 4)// [4] stands for `close` in t, ohlc
                ]);
            }
            $scope.chartConfig.series[2].data = open_sma_data;
            $scope.chartConfig.series[3].data = close_sma_data;
        }

        function update_smas_visibility () {
            $scope.chartConfig.series[2].visible = $scope.show_open_sma;
            $scope.chartConfig.series[3].visible = $scope.show_close_sma;
        }

        function update_emas_visibility () {
            $scope.chartConfig.series[4].visible = $scope.show_open_ema;
            $scope.chartConfig.series[5].visible = $scope.show_close_ema;
        }

        function update_chart () {
            $scope.chartConfig.series[0].data = [];
            $scope.chartConfig.series[1].data = [];
            $scope.chartConfig.series[0].name = $scope.market;
            $scope.candlesticks.forEach(function (item) {
                $scope.chartConfig.series[0].data.push([new Date(item.time).getTime(),
                    +item.open, +item.max, +item.min, +item.close]);
                $scope.chartConfig.series[1].data.push([new Date(item.time).getTime(), +item.volume]);
            });

            update_smas();
            update_smas_visibility();

            update_emas();
            update_emas_visibility();
        }

        $scope.list = function () {
            load_candlesticks_or_use_cached($scope.market, $scope.currency, $scope.period, update_chart);
        };

        function load_candlesticks_or_use_cached (market, currency, period, callback) {
            callback = callback || function () {};
            if ($scope.candlestick_cache[market][currency][period].updated) {
                // TODO: invalidate cache after some time
                $scope.candlesticks = $scope.candlestick_cache[market][currency][period].data;
                callback();
            } else {
                $scope.loading = true;
                $scope.chartConfig.loading = true;
                Candlesticks.get({
                        market: market,
                        currency: currency,
                        period: period,
                        limit: 200
                    },
                    function (res) {
                        $scope.loading = false;
                        $scope.candlestick_cache[market][currency][period].data = $scope.candlesticks = res.rows;
                        $scope.candlestick_cache[market][currency][period].updated = new Date();
                        // TODO: figure out which counters are useful here and which are not
//                        $scope.count = res.count;
//                        $scope.row_count = res.row_count;
//                        $scope.limit = res.limit;
//                        $scope.offset = res.offset;
//                        $scope.page = res.page;
//                        $scope.per_page = res.total_pages;
                        callback();
                        $scope.chartConfig.loading = false;
                    }
                );
            }
        }

        $scope.params = $routeParams;

        $scope.chartConfig = {
            options: {
                global: {
                    useUTC: false
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        animation: false
                    },
                    area: {
                        fillOpacity: .2
                    },
                    candlestick: {
//                        upColor: 'rgba(168,  212,  60,  .8)',
//                        dataGrouping: {
//                            enabled: false,
//                            groupPixelWidth: 3
//                        },
//                        color: [
//                            'rgba(255,  85,  46,  .8)'
//                        ]
                    }
                },

//                rangeSelector : {
//                    selected : 1
//                },
                legend: {
                    enabled: false
                },

                scrollbar: {
                    enabled: false
                },

                navigation: {
                    enabled: false
                },

                rangeSelector: {
                    enabled: false
                },

//                navigator: {
//                    enabled: true
//                },

                yAxis: [{
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: 'OHLC',
                        enabled: false
                    },
                    height: '90%',
                    lineWidth: 2
                }, {
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: 'Volume',
                        enabled: false
                    },
                    top: '90%',
                    height: '10%',
                    offset: 0,
                    lineWidth: 2
                }]
            },

            series: [
                {
                    type: $cookies.candlesticks_graph_mode || 'candlestick',
                    name: 'HitBTC',
                    data: []
    //                dataGrouping: {
    //                    units: [[
    //                        'week',                         // unit name
    //                        [1]                             // allowed multiples
    //                    ], [
    //                        'month',
    //                        [1, 2, 3, 4, 6]
    //                    ]]
    //                }
                },
                {
                    type: 'column',
                    name: 'Volume',
                    data: [],
                    yAxis: 1
    //                dataGrouping: {
    //                    units: [[
    //                        'week',                         // unit name
    //                        [1]                             // allowed multiples
    //                    ], [
    //                        'month',
    //                        [1, 2, 3, 4, 6]
    //                    ]]
    //                }
                },
                {
                    name: 'Open SMA',
                    type: 'line',
                    yAxis: 0,
                    color: 'rgba(155, 255, 5, .4)', // TODO: find better colors
                    visible: false
                },
                {
                    name: 'Close SMA',
                    type: 'line',
                    yAxis: 0,
                    color: 'rgba(189, 68, 239, .4)',
                    visible: false
                },
                {
                    name: 'Open EMA',
                    type: 'spline',
                    yAxis: 0,
                    color: 'rgba(255, 155, 5, .4)',
                    visible: false
                },
                {
                    name: 'Close EMA',
                    type: 'spline',
                    yAxis: 0,
                    color: 'rgba(68, 189, 239, .4)',
                    visible: false
                }],

            loading: true,

            useHighStocks: true

        };

    });