'use strict';

angular.module('insight.news').controller('NewsController',
    function ($scope, $rootScope, $routeParams, $location, Global, News) {

        $scope.global = Global;
        $scope.loading = false;

        $scope.list = function (limit) {
            $scope.loading = true;

            $rootScope.titleDetail = $scope.detail;

            News.get(function (res) {
                $scope.loading = false;
                $scope.news = !limit ? res.news : res.news.slice(0, limit);
                $scope.htmlReady();
            });

        };

        $scope.params = $routeParams;

    });
