'use strict';

var TRANSACTION_DISPLAYED = 10;
var BLOCKS_DISPLAYED = 5;
var MAX_TXS_ON_CHART = 42;

angular.module('insight.system').controller('IndexController',
    function ($scope, Global, getSocket, Blocks, TransactionSnapshot) {
        $scope.global = Global;

        var _getBlocks = function () {
            Blocks.get({
                limit: BLOCKS_DISPLAYED
            }, function (res) {
                $scope.blocks = res.blocks;
                $scope.blocksLength = res.length;
            });
        };

        var socket = getSocket($scope);

        var _startSocket = function () {
            socket.emit('subscribe', 'inv');

            socket.on('block', function () {
                _getBlocks();
            });
        };

        socket.on('connect', function () {
            _startSocket();
        });

        var _load_tx_snapshot = function () {
            $scope.tx_snapshot_loading = true;

            TransactionSnapshot.get(
                function (res) {
                    $scope.loading = false;
                    $scope.tx_snapshot = res.transactions;
                    $scope.tx_snapshot_size = res.length;
//                    console.log('snapshot loaded', $scope.tx_snapshot_size, $scope.tx_snapshot);
                    $scope.txs = $scope.tx_snapshot;
                    if (parseInt($scope.txs.length, 10) >= parseInt(TRANSACTION_DISPLAYED, 10)) {
                        $scope.txs = $scope.txs.splice(0, TRANSACTION_DISPLAYED);
                    }
                    $scope.txChartConfig.series[0].data = [];
                    for (var key = 0; key < $scope.tx_snapshot.length; key++) {
                        $scope.txChartConfig.series[0].data.push($scope.tx_snapshot[key].valueOut);
                    }
                    $scope.txChartConfig.loading = false;

                    socket.on('tx', function (tx) {
                        $scope.txs.unshift(tx);
                        if (+$scope.txs.length >= TRANSACTION_DISPLAYED) {
                            $scope.txs = $scope.txs.splice(0, TRANSACTION_DISPLAYED);
                        }
                        if ($scope.txChartConfig.series[0].data.length - MAX_TXS_ON_CHART > 1) {
                            $scope.txChartConfig.series[0].data.splice(0,
                                $scope.txChartConfig.series[0].data.length - (MAX_TXS_ON_CHART + 1));
                        }
                        $scope.txChartConfig.series[0].data.push(tx.valueOut);
                    });

                    $scope.htmlReady();
                }
            );

        };

        $scope.txChartConfig = {
            options: {
                chart: {
                    type: 'column',
//                    animation: Highcharts.svg
                    animation: {
                        duration: 0
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b><br />',
                    pointFormat: '{point.y} BTC'
                },
                legend: {
                    enabled: false
                }
            },
            title: {
                text: 'Latest Transactions'
            },
            loading: true,
            xAxis: {
                type: null,
                labels: {
                    enabled: false
                }
            },
            yAxis: {
                type: 'logarithmic',
                minorTickInterval: 0.1,
                labels: {
                    enabled: true
                },
                title: {
                    text: 'BTC amount',
                    enabled: false
                }
            },
            series: [{
                name: 'BTC amount',
                data: [],
                pointStart: 1
//                color: '#6C9032'
            }]
        };


        $scope.humanSince = function (time) {
            var m = moment.unix(time);
            return m.max().fromNow();
        };

        $scope.index = function () {
            _getBlocks();
            _load_tx_snapshot();
            _startSocket();
        };

        $scope.txs = [];
        $scope.blocks = [];
    });