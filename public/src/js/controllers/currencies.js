'use strict';

angular.module('insight.currencies', ["highcharts-ng"]).controller('CurrenciesController',
    function ($scope, $rootScope, $routeParams, $cookies, $location, Global, Candlesticks) {

        $scope.global = Global;
        $scope.loading = false;
        $scope.market = $cookies.currencies_market || 'all';
        $scope.currency = $cookies.currencies_currency || 'usd';
        $scope.period = $cookies.currencies_period || 'day';
        $scope.view_by = $cookies.currencies_view_by || 'price';
        $scope.candlesticks = [];
        $scope.multimarket_mode = false;

        $rootScope.meta_keywords = 'btc, usd, exchange, trades, digital currency, bitcoin, btc, bitcoin price, bitcoin price graph';
        $rootScope.meta_description = 'Adjustable Bitcoin price graphics: flexible filters, data periods, and multiple exchanges.';


        $scope.candlestick_cache = {
            btce: {
                'usd': {
                    'minute': {updated: null, data: []},
                    '5minutes': {updated: null, data: []},
                    '15minutes': {updated: null, data: []},
                    '30minutes': {updated: null, data: []},
                    'hour': {updated: null, data: []},
                    '4hours': {updated: null, data: []},
                    '8hours': {updated: null, data: []},
                    '12hours': {updated: null, data: []},
                    'day': {updated: null, data: []},
                    '10days': {updated: null, data: []},
                    '20days': {updated: null, data: []}
                },
                'eur': {
                    'minute': {updated: null, data: []},
                    '5minutes': {updated: null, data: []},
                    '15minutes': {updated: null, data: []},
                    '30minutes': {updated: null, data: []},
                    'hour': {updated: null, data: []},
                    '4hours': {updated: null, data: []},
                    '8hours': {updated: null, data: []},
                    '12hours': {updated: null, data: []},
                    'day': {updated: null, data: []},
                    '10days': {updated: null, data: []},
                    '20days': {updated: null, data: []}
                }
            },
            hitbtc: {
                'usd': {
                    'minute': {updated: null, data: []},
                    '5minutes': {updated: null, data: []},
                    '15minutes': {updated: null, data: []},
                    '30minutes': {updated: null, data: []},
                    'hour': {updated: null, data: []},
                    '4hours': {updated: null, data: []},
                    '8hours': {updated: null, data: []},
                    '12hours': {updated: null, data: []},
                    'day': {updated: null, data: []},
                    '10days': {updated: null, data: []},
                    '20days': {updated: null, data: []}
                },
                'eur': {
                    'minute': {updated: null, data: []},
                    '5minutes': {updated: null, data: []},
                    '15minutes': {updated: null, data: []},
                    '30minutes': {updated: null, data: []},
                    'hour': {updated: null, data: []},
                    '4hours': {updated: null, data: []},
                    '8hours': {updated: null, data: []},
                    '12hours': {updated: null, data: []},
                    'day': {updated: null, data: []},
                    '10days': {updated: null, data: []},
                    '20days': {updated: null, data: []}
                }
            },
            bitstamp: {
                'usd': {
                    'minute': {updated: null, data: []},
                    '5minutes': {updated: null, data: []},
                    '15minutes': {updated: null, data: []},
                    '30minutes': {updated: null, data: []},
                    'hour': {updated: null, data: []},
                    '4hours': {updated: null, data: []},
                    '8hours': {updated: null, data: []},
                    '12hours': {updated: null, data: []},
                    'day': {updated: null, data: []},
                    '10days': {updated: null, data: []},
                    '20days': {updated: null, data: []}
                },
                'eur': {
                    'minute': {updated: null, data: []},
                    '5minutes': {updated: null, data: []},
                    '15minutes': {updated: null, data: []},
                    '30minutes': {updated: null, data: []},
                    'hour': {updated: null, data: []},
                    '4hours': {updated: null, data: []},
                    '8hours': {updated: null, data: []},
                    '12hours': {updated: null, data: []},
                    'day': {updated: null, data: []},
                    '10days': {updated: null, data: []},
                    '20days': {updated: null, data: []}
                }
            }
        };

        angular.forEach(['market', 'currency', 'period'], function (key) {
            $scope.$watch(key, function (new_value, old_value) {
                if (new_value != old_value) {
                    $cookies['currencies_' + key] = new_value;
                    $scope.list();
                }
            });
        });

        $scope.markets_available = [
            {
                id: 'all',
                title: 'All',
                currencies: ['usd']
            },
            {
                id: 'btce',
                title: 'BTCE',
                currencies: ['usd', 'eur']
            },
            {
                id: 'hitbtc',
                title: 'HitBTC',
                currencies: ['usd', 'eur']
            },
            {
                id: 'bitstamp',
                title: 'BitStamp',
                currencies: ['usd']
            }
        ];

        var single_market_mode_view_modes_available = [
            {id: 'all', title: 'All markers'},
            {id: 'price', title: 'Avg. Price'},
            {id: 'volume', title: 'Volume'},
            {id: 'currency_volume', title: 'Currency volume'},
            {id: 'count', title: 'Trade count'},
            {id: 'open', title: 'Open'},
            {id: 'close', title: 'Close'},
            {id: 'min', title: 'Min'},
            {id: 'max', title: 'Max'}
        ];

        $scope.view_modes_available = single_market_mode_view_modes_available;

        var multi_market_mode_view_modes_available = [
            {id: 'price', title: 'Avg. Price'},
            {id: 'volume', title: 'Volume'},
            {id: 'count', title: 'Trade count'},
            {id: 'open', title: 'Open'},
            {id: 'close', title: 'Close'},
            {id: 'min', title: 'Min'},
            {id: 'max', title: 'Max'}
        ];

        $scope.periods_available = [
            {id: '20days', title: '20 days'},
            {id: '10days', title: '10 days'},
            {id: 'day', title: 'day'},
            {id: '12hours', title: '12 hours'},
            {id: '8hours', title: '8 hours'},
            {id: '4hours', title: '4 hours'},
            {id: 'hour', title: 'hour'},
            {id: '30minutes', title: '30 minutes'},
            {id: '15minutes', title: '15 minutes'},
            {id: '5minutes', title: '5 minutes'},
            {id: 'minute', title: '1 minute'}
        ];

        $scope.graph_modes_available = [
            {id: 'spline', title: 'Spline graph'},
            {id: 'line', title: 'Line graph'},
            {id: 'bar', title: 'Bar graph'},
            {id: 'column', title: 'Column graph'}
        ];

        $scope.$watch('market', function (new_value, old_value) {
            if (new_value != old_value) {
                $cookies.currencies_market = new_value;

                $scope.multimarket_mode = new_value == 'all';
                if ($scope.multimarket_mode) {
                    $scope.view_modes_available = multi_market_mode_view_modes_available;
                } else {
                    $scope.view_modes_available = single_market_mode_view_modes_available;
                }
                for (i=0; i < $scope.view_modes_available.length; i++) {
                    if ($scope.view_modes_available[i].id == $scope.view_by)
                        break;
                }
                if (i >= $scope.view_modes_available.length) {
                    $scope.view_by = $scope.view_modes_available[0].id;
                }
            }

            $scope.currencies_available = [];
            // select the first available currency if current currency is invalid after market choice change
            for (var i=0; i < $scope.markets_available.length; i++) {
                if ($scope.markets_available[i].id == $scope.market) {
                    $scope.currencies_available = $scope.markets_available[i].currencies;
                    if (-1 == $scope.currencies_available.indexOf($scope.currency)) {
                        $scope.currency = $scope.currencies_available[0];
                    }
                    return;
                }
            }
        });

        function update_chart () {
            if ($scope.multimarket_mode) {
                $scope.chartConfig.series = [
                    {
                        name: 'Btce ' + $scope.view_by + ' in ' + $scope.currency,
                        market: 'btce',
                        data: []//$scope.candlestick_cache['btce'][$scope.currency][$scope.period].data
                    },
                    {
                        name: 'Hitbtc ' + $scope.view_by + ' in ' + $scope.currency,
                        market: 'hitbtc',
                        data: []//$scope.candlestick_cache['hitbtc'][$scope.currency][$scope.period].data
                    },
                    {
                        name: 'Bitstamp ' + $scope.view_by + ' in ' + $scope.currency,
                        market: 'bitstamp',
                        data: []//$scope.candlestick_cache['bitstamp'][$scope.currency][$scope.period].data
                    }
                ]; // by number of markets we have here
                for (var key = 0; key < $scope.chartConfig.series.length; key++) {
                    var s = $scope.chartConfig.series[key];
                    if (!s.data.length) {
                        (function wrap (key, s) {
                            load_candlesticks_or_use_cached(s.market, $scope.currency, $scope.period, function () {
                                $scope.candlestick_cache[s.market][$scope.currency][$scope.period].data.forEach(function (item) {
                                    s.data.push([new Date(item.time).getTime(), parseFloat(item[$scope.view_by])]);
                                });
                            });
                        }) (key, s);
                    }
                    // for every market load data or get from cache
                    // on data loaded if multimarket_mode is still on, update chart
                }
            }
            else {
                if ($scope.view_by == 'all') {
                    $scope.chartConfig.series = [
                        {data: []}, {data: []}, {data: []}, {data: []}, {data: []} // by number of metrics we have here
    //                    {data: []}, {data: []} // count and volume
                    ];
                } else {
                    $scope.chartConfig.series = [{data: []}];
                }
                $scope.candlesticks.forEach(function (item) {
                    if ($scope.view_by == 'all') {
                        $scope.chartConfig.series[0].data.push([new Date(item.time).getTime(), parseFloat(item['price'])]);
                        $scope.chartConfig.series[1].data.push([new Date(item.time).getTime(), parseFloat(item['min'])]);
                        $scope.chartConfig.series[2].data.push([new Date(item.time).getTime(), parseFloat(item['max'])]);
                        $scope.chartConfig.series[3].data.push([new Date(item.time).getTime(), parseFloat(item['open'])]);
                        $scope.chartConfig.series[4].data.push([new Date(item.time).getTime(), parseFloat(item['close'])]);
    //                    $scope.chartConfig.series[5].data.push([new Date(item.time).getTime(), parseFloat(item['volume'])]);
    //                    $scope.chartConfig.series[6].data.push([new Date(item.time).getTime(), parseFloat(item['count'])]);
                    } else {
                        $scope.chartConfig.series[0].data.push([new Date(item.time).getTime(), parseFloat(item[$scope.view_by])]);
                    }

                });
                if ($scope.view_by == 'all') {
                    $scope.chartConfig.series[0].name = 'BTC Avg. Price in ' + $scope.currency;
                    $scope.chartConfig.series[1].name = 'BTC Min in ' + $scope.currency;
                    $scope.chartConfig.series[2].name = 'BTC Max in ' + $scope.currency;
                    $scope.chartConfig.series[3].name = 'BTC Open in ' + $scope.currency;
                    $scope.chartConfig.series[4].name = 'BTC Close in ' + $scope.currency;
    //                $scope.chartConfig.series[5].name = 'BTC Volume in ' + $scope.currency;
    //                $scope.chartConfig.series[6].name = 'BTC Count in ' + $scope.currency;
                } else {
                    $scope.chartConfig.series[0].name = 'BTC ' + $scope.view_by + ' in ' + $scope.currency;
                }
    //            $scope.chartConfig.title.text
    //                = $scope.market + ', ' + $scope.currency + ', ' + $scope.period + ', ' + $scope.view_by;
            }
        }

        $scope.$watch('view_by', function (new_value, old_value) {
            if (new_value != old_value) {
                $cookies.currencies_view_by = new_value;
                $scope.chartConfig.loading = true;
                update_chart();
                $scope.chartConfig.loading = false;
            }
        });

        $scope.$watch('chartConfig.options.chart.type', function (new_value, old_value) {
            if (new_value != old_value) {
                $cookies.currencies_graph_mode = new_value;
            }
        });

        $scope.list = function () {
            $scope.multimarket_mode = ($scope.market == 'all');
            if ($scope.multimarket_mode) {
                update_chart();
            } else {
                load_candlesticks_or_use_cached($scope.market, $scope.currency, $scope.period, update_chart);
            }

        };

        function load_candlesticks_or_use_cached (market, currency, period, callback) {
            callback = callback || function () {};
            if ($scope.candlestick_cache[market][currency][period].updated) {
                // TODO: invalidate cache after some time
                $scope.candlesticks = $scope.candlestick_cache[market][currency][period].data;
                callback();
            } else {
                $scope.loading = true;
                $scope.chartConfig.loading = true;
                Candlesticks.get({
                        market: market,
                        currency: currency,
                        period: period,
                        limit: 1000
                    },
                    function (res) {
                        $scope.loading = false;
                        $scope.candlestick_cache[market][currency][period].data = $scope.candlesticks = res.rows;
                        $scope.candlestick_cache[market][currency][period].updated = new Date();
                        // TODO: figure out which counters are useful here and which are not
//                        $scope.count = res.count;
//                        $scope.row_count = res.row_count;
//                        $scope.limit = res.limit;
//                        $scope.offset = res.offset;
//                        $scope.page = res.page;
//                        $scope.per_page = res.total_pages;
                        $scope.chartConfig.loading = false;
                        callback();
                    }
                );
            }
        }

        $scope.params = $routeParams;

        $scope.chartConfig = {
            options: {
                global: {
                    useUTC: false
                },
                chart: {
                    type: $cookies.currencies_graph_mode || 'spline',
                    zoomType: 'x'
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        animation: false
                    },
                    area: {
                        fillOpacity: .2
                    },
                    line: {
                        marker: {
                            enabled: false
                        }
                    },
                    spline: {
                        marker: {
                            enabled: false
                        }
                    }
                }
//                colors: [  // a palitre from older Chainy, not used anymore
//                    "rgb(75,75,255)",
//                    "rgb(213,118,255)",
//                    "rgb(240,117,117)",
//                    "rgb(255,255,210)",
//                    "rgb(170,233,235)",
//                    "rgb(202,255,75)",
//                    "rgb(255,180,105)",
//                    "rgb(255,202,155)",
//                    "rgb(175,224,255)",
//                    "rgb(255,95,135)",
//                    "rgb(75,255,255)",
//                    "rgb(75,75,214)",
//                    "rgb(75,214,214)",
//                    "rgb(255,209,86)",
//                    "rgb(244,244,244)",
//                    "rgb(75,175,75)",
//                    "rgb(255,95,222)",
//                    "rgb(248,255,122)"
//                ]
            },
            legend: {
                enabled: false
            },
            series: [{
                data: []
            }],
            title: {
                text: '',
                enabled: false
            },

            loading: true,

            useHighStocks: true,

            scrollbar: {
                enabled: false
            },

            navigator: {
                enabled: true
            },

            rangeSelector: {
                enabled: false
            },

            xAxis: {
                type: 'datetime'
            },
            yAxis: {

            }
        };

    });
