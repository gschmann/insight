angular.module('insight.candlesticks').controller('ChartSettingsController',
    function ($scope, $modalInstance,
              show_open_sma, show_close_sma, sma_period,
              show_open_ema, show_close_ema, smooth_ema,
              orderbook_price_grouping_step, orderbook_price_grouping_steps_available, orderbook_count, trade_count) {

        $scope.data = {
            show_open_sma: show_open_sma,
            show_close_sma: show_close_sma,
            sma_period: sma_period,

            show_open_ema: show_open_ema,
            show_close_ema: show_close_ema,
            smooth_ema: smooth_ema,

            orderbook_price_grouping_steps_available: orderbook_price_grouping_steps_available,
            orderbook_price_grouping_step: orderbook_price_grouping_step,
            orderbook_count: orderbook_count,
            trade_count: trade_count
        };
    
        $scope.ok = function () {
            $modalInstance.close({
                show_open_sma: $scope.data.show_open_sma,
                show_close_sma: $scope.data.show_close_sma,
                sma_period: $scope.data.sma_period,

                show_open_ema: $scope.data.show_open_ema,
                show_close_ema: $scope.data.show_close_ema,
                smooth_ema: $scope.data.smooth_ema,

                orderbook_price_grouping_step: $scope.data.orderbook_price_grouping_step,
                orderbook_count: $scope.data.orderbook_count,
                trade_count: $scope.data.trade_count
            });
        };
    
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }
);