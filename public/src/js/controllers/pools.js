'use strict';

angular.module('insight.pools', ["highcharts-ng"]).controller('PoolsController',
    function ($scope, $rootScope, $routeParams, $location, Global, Pools) {

        $scope.global = Global;
        $scope.pool_data = [];

        $rootScope.meta_keywords = 'bitcoin, btc, mining pool, bitcoin pool, bitcoin pool information, hashrate, cryptocurrency';
        $rootScope.meta_description = 'The largest Bitcoin mining pools in the world. Whole Bitcoin mining information on a single chart.';

        $scope.sort = {
            column: '',
            descending: false
        };

        $scope.changeSorting = function(column) {
            var sort = $scope.sort;

            if (sort.column == column) {
                sort.descending = !sort.descending;
            } else {
                sort.column = column;
                sort.descending = false;
            }
        };

        $scope.list = function () {
            $scope.pieChartConfig.loading = true;
            Pools.get(function (res) {
                $scope.pool_data = [];
                $scope.pieChartConfig.series[0].data = [];
                for (var i in res.pool_data) {
                    if (!res.pool_data.hasOwnProperty(i)) continue;
                    $scope.pieChartConfig.series[0].data.push([
                            res.pool_data[i].pool_name || 'unknown',
                            +res.pool_data[i].blocks_mined
                    ]);
                    $scope.pool_data.push({
                        pool_name: res.pool_data[i].pool_name,
                        blocks_mined: +res.pool_data[i].blocks_mined
                    })
                }
                $scope.pieChartConfig.loading = false;
                $scope.htmlReady();
            });
        };

        $scope.pieChartConfig = {
            title: {
                text: 'Blocks mined per pool',
                enabled: false
            },
            loading: false,
            options: {
                chart: {
                    type: 'pie'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
//                        dataLabels: { enabled: false },
                        showInLegend: true
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Blocks mined',
                data: []
            }]
        };
    });
