'use strict';

// TODO: use Angular.forEach wherever possible instead of "for (key in dict)"

angular.module('insight.block-headers').controller('BlockHeadersController',
    function ($scope, $rootScope, $routeParams, $location, Global, BlockHeaders, Pools) {

        $scope.global = Global;
        $scope.block_headers = {
            rows: [],
            pagination: {
                total: 1,
                current_page: 1,
                per_page: 50,
                total_pages: 1
            },
            pagination_representation: [1],
            filtration: {
                height__gte: '',
                height__lte: '',
                size__gte: '',
                size__lte: '',
                txlength__gte: '',
                txlength__lte: '',
                pool_name: ''
            }
        };

        if ($routeParams.poolName) {
            $scope.block_headers.filtration.pool_name = $rootScope.poolName = $routeParams.poolName;
        }

        $scope.pools_available = [];

        $scope.sort = {
            column: '',
            descending: false
        };

        $scope.changeSorting = function(column) {
            var sort = $scope.sort;

            if (sort.column == column) {
                sort.descending = !sort.descending;
            } else {
                sort.column = column;
                sort.descending = false;
            }
        };

        angular.forEach([
            'block_headers.filtration.height__gte',
            'block_headers.filtration.height__lte',
            'block_headers.filtration.size__gte',
            'block_headers.filtration.size__lte',
            'block_headers.filtration.txlength__gte',
            'block_headers.filtration.txlength__lte'
        ], function (key) {
            $scope.$watch(key, function (new_value, old_value) {
                if (new_value != old_value) {
                    $scope.list(1);
                }
            });
        });

        $scope.list = function (page) {
            if (!$scope.pools_available.length) {
                Pools.get(function (res) {
                    $scope.pools_available = [];
                    for (var i in res.pool_data) {
                        if (!res.pool_data.hasOwnProperty(i)) continue;
                        $scope.pools_available.push(res.pool_data[i].pool_name)
                    }
                    if ($routeParams.poolName) {
                        $scope.block_headers.filtration.pool_name = $rootScope.poolName = $routeParams.poolName;
                    }
                });
            }

            page = page || 1;
            var filter_and_pagination = {
                page: page,
                per_page: 50 // TODO: move to config
            };
            for (var key in $scope.block_headers.filtration) {
                if (!$scope.block_headers.filtration.hasOwnProperty(key) || $scope.block_headers.filtration[key] === '')
                    continue;
                filter_and_pagination[key] = $scope.block_headers.filtration[key];
            }

            BlockHeaders.get(filter_and_pagination, function (res) {
                $scope.block_headers.rows = res.block_headers;
                $scope.block_headers.pagination.total = res.total;
                $scope.block_headers.pagination.current_page = res.page;
                $scope.block_headers.pagination.per_page = res.per_page;
                $scope.block_headers.pagination.total_pages = res.total_pages;
                update_pagination_representation();
                $scope.htmlReady();
            });
        };

        $scope.filter_by_pool = function (pool_name) {
            $scope.block_headers.filtration.pool_name = pool_name;
            $scope.list();
        };

        function add_page_to_representation_if_not_there_yet(i) {
            if ($scope.block_headers.pagination_representation.indexOf(i) == -1)
                $scope.block_headers.pagination_representation.push(i);
        }

        // TODO: check http://angular-ui.github.io/bootstrap/ out
        function update_pagination_representation () {
            var N = 3,
                total = $scope.block_headers.pagination.total_pages,
                current = $scope.block_headers.pagination.current_page;
            $scope.block_headers.pagination_representation = [];
            if (total <= 1) return;
            for (var i = 1; i <= N && i <= total; i++)
                add_page_to_representation_if_not_there_yet(i);

            if (i > total) return;
            if (current - N + 1 > i) {
                i = current + 1 - N;
                $scope.block_headers.pagination_representation.push(' ...'); // don't remove the space
            }

            for (; i <= current + N - 1 && i <= total; i++)
                add_page_to_representation_if_not_there_yet(i);

            if (i > total) return;
            if (total - N + 1 > i) {
                i = total + 1 - N;
                $scope.block_headers.pagination_representation.push('... '); // keep separators different
            }
            
            for (; i <= total; i++)
                add_page_to_representation_if_not_there_yet(i);
        }
    }
);
