'use strict';

angular.module('insight.system').controller('HeaderController',
    function ($scope, $rootScope, $modal, getSocket, Global, Block) {
        $scope.global = Global;

        if (typeof String.prototype.startsWith != 'function') {
            String.prototype.startsWith = function (str){
                return this.slice(0, str.length) == str;
            };
        }

        $rootScope.is_marketchart = false;
        $rootScope.is_blockexplorer = false;

        setInterval(function () {
            $rootScope.is_marketchart = (location.pathname == '/currencies' || location.pathname == '/markets');
            $rootScope.is_blockexplorer = (location.pathname.startsWith('/block')
                || location.pathname.startsWith('/tx') || location.pathname.startsWith('/addr'))
        }, 100);

        $rootScope.currency = {
            factor: 1,
            bitstamp: 0,
            symbol: 'BTC'
        };

//        $scope.menu = [
//            {
//                'title': 'Block Explorer',
//                'link': 'blocks'
//            },
//            {
//                'title': 'Pools',
//                'link': 'pools'
//            },
////            {
////                'title': 'News',
////                'link': 'news'
////            },
//            {
//                'title': 'Currencies',
//                'link': 'currencies'
//            },
//            {
//                'title': 'Markets',
//                'link': 'markets'
//            }
//        ];

        $scope.openScannerModal = function () {
            var modalInstance = $modal.open({
                templateUrl: 'scannerModal.html',
                controller: 'ScannerController'
            });
        };

        var _getBlock = function (hash) {
            Block.get({
                blockHash: hash
            }, function (res) {
                $scope.totalBlocks = res.height;
            });
        };

        var socket = getSocket($scope);
        socket.on('connect', function () {
            socket.emit('subscribe', 'inv');

            socket.on('block', function (block) {
                var blockHash = block.toString();
                _getBlock(blockHash);
            });
        });

        $rootScope.isCollapsed = true;
    });
