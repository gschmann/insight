'use strict';

angular.module('insight.candlesticks')
    .factory('Trades',
    function ($resource) {
        return $resource('/api/markets/trades?market=:market&currency=:currency&limit=:limit');
    });