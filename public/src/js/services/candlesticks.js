'use strict';

// TODO: use https://github.com/marmorkuchen-net/angular-watch-resource
// NOTE: uncomment https://bitcoinchain.com from urls for local testing with production database
angular.module('insight.candlesticks')
    .factory('Candlesticks',
    function ($resource) {
        return $resource(//'https://bitcoinchain.com' +
            '/api/markets/candlesticks/latest?market=:market&currency=:currency&period=:period&limit=:limit');
    });

angular.module('insight.currencies')
    .factory('Candlesticks',
    function ($resource) {
        return $resource(//'https://bitcoinchain.com' +
            '/api/markets/candlesticks/latest?market=:market&currency=:currency&period=:period&limit=:limit');
    });
