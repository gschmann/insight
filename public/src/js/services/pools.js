'use strict';

angular.module('insight.pools')
    .factory('Pools',
    function ($resource) {
        return $resource('/api/pools/blocks_mined_per_pool');
    });
