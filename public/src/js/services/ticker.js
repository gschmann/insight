'use strict';

angular.module('insight.candlesticks')
    .factory('Ticker',
    function ($resource) {
        return $resource('/api/markets/ticker?market=:market&currency=:currency');
    });