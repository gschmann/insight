'use strict';

angular.module('insight.candlesticks')
    .factory('Orderbook',
    function ($resource) {
        return $resource('/api/markets/orderbook?market=:market&currency=:currency&limit=:limit&grouping_price_step=:grouping_price_step');
    });