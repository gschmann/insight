'use strict';

angular.module('insight.block-headers')
    .factory('BlockHeaders',
    function ($resource) {
        return $resource('/api/block-headers');
    });
