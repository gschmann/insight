'use strict';

angular.module('insight.news')
    .factory('News',
    function ($resource) {
        return $resource('/api/news');
    });
