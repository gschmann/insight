'use strict';

angular.module('insight', [
    'ngAnimate',
    'ngResource',
    'ngRoute',
    'ngProgress',
    'ngCookies',
    'ui.bootstrap',
    'ui.route',
    'monospaced.qrcode',
    'gettext',
    'insight.system',
    'insight.socket',
    'insight.blocks',
    'insight.block-headers',
    'insight.news',
    'insight.pools',
    'insight.transactions',
    'insight.address',
    'insight.search',
    'insight.status',
    'insight.connection',
    'insight.currency',
    'insight.currencies',
    'insight.candlesticks',
    'seo',

    'angulartics',
    'angulartics.google.analytics'
]);

angular.module('insight.system', []);
angular.module('insight.socket', []);
angular.module('insight.blocks', []);
angular.module('insight.block-headers', []);
angular.module('insight.news', []);
angular.module('insight.pools', []);
angular.module('insight.transactions', []);
angular.module('insight.address', []);
angular.module('insight.search', []);
angular.module('insight.status', []);
angular.module('insight.connection', []);
angular.module('insight.currency', []);
angular.module('insight.candlesticks', ["highcharts-ng"]);
angular.module('insight.currencies', ["highcharts-ng"]);
angular.module('highcharts.test', ["highcharts-ng"]);
