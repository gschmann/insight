#!/bin/sh
# use it if you need to process a full update, including npm update (e.g. if you changed insight-api too)

# copy database config; normally is a overkill but still it's safer
cp node_modules/insight-bitcore-api/config/sql.json sql.json.bak

sudo service insight stop

git checkout .
git checkout master
git pull

rm -rf node_modules

npm install

cp sql.json.bak node_modules/insight-bitcore-api/config/sql.json

rm sql.json.bak

sudo service insight start

